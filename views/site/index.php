<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Aplicación';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent col-md-12">
        
        <h1 class="display-4"></h1> 
        
        <?=Html::a(Html::img("@web/images/cantabria.jpg",['class' => 'sizecantabria']),['site/about'])?> 
        
    </div>

<!--    <div class="body-content">-->
<div class = "row ">
    <div class = "split left">
        <?=Html::a(Html::img("@web/images/equipo.jpg",['class' => 'imagenleft']),['site/about'])?> 
        <div class = "centered">
            
            
                <p class = mas-grande>EQUIPOS</p>
            </div>
        
        </div>
      
    <div class = "split right">
        <?=Html::a(Html::img("@web/images/etapas.jpeg",['class' => 'imagenleft']),['site/about'])?> 
            <div class = "centered">
                <p class = mas-grande>ETAPAS</p>
            </div>
     <div>       
        
</div>
<!--    </div>-->
        
        </div>

<!--    <div class="body-content">

        <div class="split">
            <div class = "left">
            <div class = "centered">
                <p>Hola</p>
            </div>
            </div>
        </div>
        
        <div class="split right">
            <div class = "right">
            <div class = "centered">
                <p>Hola</p>
            </div>
            </div>
        </div>

    </div>-->
</div>
